var mysql = require('mysql');
var CONSTANTS = require('./static/constants.js');
var exports = module.exports = {};


var rDBConnection = mysql.createConnection({
  host: CONSTANTS.RDBMS_HOSTNAME,
  user: CONSTANTS.RDBMS_USER,
  password: CONSTANTS.RDBMS_PASSWORD,
  port : CONSTANTS.RDBMS_PORT,
  database: CONSTANTS.RDBMS_DATABASE
});

  
exports.connectToDB = function (callbackFn) {
  rDBConnection.connect(function(err){
    if(err){
      console.log('Error connecting to Db' + err) ;
    }
    callbackFn(err, rDBConnection);
    console.log('Connection established');
  });
}


exports.getConnection = function () {
    return rDBConnection;
 }


// handle when the DB is disconnected


