var exports = module.exports = {};
var CONSTANTS = require('./constants.js');

/*
 * http://stackoverflow.com/questions/7244246/generate-an-rfc-3339-timestamp-similar-to-google-tasks-api
 */ 
 
exports.pad = function (n){return n<10 ? '0' + n : n} 


exports.getRandomBetween =  function (min, max) {
         return Math.random() * (max - min) + min;
}
 
exports.convertDateToRFC3339Format = function (d) {
 var pad = this.pad;
 return d.getUTCFullYear()+'-'
      + pad(d.getUTCMonth() + 1)+'-'
      + pad(d.getUTCDate())+'T'
      + pad(d.getUTCHours())+':'
      + pad(d.getUTCMinutes())+':'
      + pad(d.getUTCSeconds())+'Z';
 }


 exports.formatTimeForQuery  = function (year, month, day, hour, min, seconds, milliseconds) {
    return this.convertDateToRFC3339Format(new Date(Date.UTC(year, month, day, hour, min, seconds, milliseconds)));
 }

 exports.isDateValidForDataService = function  (date) {
    
    var monthDays = [-1, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    var isValid = true;
 	if (date) {
 
         if (date.includes('.')) {
            isValid = false;
         }

 	       var splitDate = date.split(CONSTANTS.DATE_SEPARATOR);
         //console.log("I am here");
         if (isValid && splitDate.length != 3) {
              //console.log("Length is not equal to 3");
              isValid = false;
         }
     	   if (isValid && splitDate.length == 3) {
     	   	   var year =  splitDate[2];
     	   	   var month = splitDate[1];
     	   	   var day  =  splitDate[0];
             isValid = true;
         }    

         if (isValid && (isNaN(year) ||  isNaN(month) || isNaN(day))) {
             //console.log("year is not a number");
             isValid = false;
         } 



 	   	   if (isValid && !(month > 0 && month < 13)) {
             //console.log("Month not between 1 and 12");
             isValid = false;
 	   	   }

 	   	   if (isValid && !(day > 0 && day <= monthDays[month])) {
             //console.log("day not in valid range");
             isValid = false;
 	   	   }

 	   	   if (isValid && !(year > CONSTANTS.REFYEAR && year <= 3000)) {
             //console.log("Entered a Ref Year");
             isValid = false;
 	   	   }

 	   	   // check if date is leap year we don't have that data entry

 	   	   if (isValid && (month == 2 && day >= 29)) {
               //console.log("Leap year");
               isValid = false;
 	   	   }
 	   }

      //console.log('returnning ' + isValid);
      return isValid;
     
 	}
 
 


 exports.prepareEmailBody  = function (result) {
  var body = "";
  if (result == undefined || result == null) {
      body = "";
  }
  else if (result.message  == CONSTANTS.ERR_MESSAGE) {
            body = result.message;
  } else {
            body = result.message + CONSTANTS.NEW_LINE_CHARACTER + CONSTANTS.NEW_LINE_CHARACTER;
            if(result.data.length == 0) {
               if (result.message  !== CONSTANTS.ERR_MESSAGE) {
                      body = CONSTANTS.SAFE_MESSAGE;
               }       
            } else {
              for (var i = 0; i < result.data.length; i++) {
                  body+= result.data[i] + CONSTANTS.NEW_LINE_CHARACTER;
               }
            }   
  }
  return body;
}
  