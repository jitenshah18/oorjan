var  influxDB = require('./../db.js');
var CONSTANTS = require('./../static/constants.js');
var mysql = require('../mysqlDB.js');
var mysqlDBConnection;
var args = process.argv.slice(2);

if(args.length == 0) {
  console.log("Enter node moveReferenceDataToInfluxDB <fileName>");
  process.exit();
}

var fileName = args[0];
var jsonRefData = require('./../referenceData/' + fileName);


influxDB.connectToInfluxDB();

mysql.connectToDB(startScript);


function startScript (err, sqlDBConnection) {
  if (err) {
      console.log(CONSTANTS.CANNOT_CONNECT_TO_RDBMS);
      process.exit(-1);
  }


  mysqlDBConnection = sqlDBConnection;
  insertMetaData();
}

  function insertMetaData () {
      var post  = {latitude: jsonRefData.inputs.lat, longitude: jsonRefData.inputs.lon, systemCapacity : jsonRefData.inputs.system_capacity};
      var query = mysqlDBConnection.query('INSERT INTO User_MetaData SET ?', post, function(err, result) {
        if (err) throw err;
        var installationId = result.insertId;
         console.log("installationId " + installationId);
         insertSSC_Info(installationId);

      });
     

  } 

  function insertSSC_Info (installationId) {
     var version = jsonRefData.ssc_info.version;
     var build = jsonRefData.ssc_info.build;
     var ssc_info_id;

     var query = 'SELECT * FROM ssc_info where  build = \'' + build + '\' and version = \'' + version + '\'';
     mysqlDBConnection.query(query, function(err,rows){
      if(err) throw err;
      if (rows.length == 0) {
        // inser this new info
         var post  = {version: version, build : build};
         query = mysqlDBConnection.query('INSERT INTO ssc_info SET ?', post, function(err, result) {
              if (err) throw err;
              ssc_info_id = result.insertId;
          });


      } else {
        ssc_info_id = rows[0].ssc_info_id;
        console.log("ssc_info_id already registered " + ssc_info_id);
      }
      insertStation_Info(installationId, ssc_info_id);
    });
    
    
  }

  function insertStation_Info (installationId, ssc_info_id) {


     var lat = jsonRefData.station_info.lat;
     var lon = jsonRefData.station_info.lon;
     var elev = jsonRefData.station_info.elev;
     var tz = jsonRefData.station_info.tz;
     var location = jsonRefData.station_info.location;
     var city = jsonRefData.station_info.city;
     var state = jsonRefData.station_info.state;
     var solar_resource_file = jsonRefData.station_info.solar_resource_file;
     var distance = jsonRefData.station_info.distance;
     var station_info_id;

     console.log(  lat + " "  + lon + " " + elev + " " + tz + " " + location + " " + city +  " " + state + " " + solar_resource_file + " " 
       +  distance);

     var query = 'SELECT * FROM station_info where  lat = ' + lat 
                          + ' and lon = ' + lon 
                          + ' and elev = ' + elev 
                          + ' and tz = ' + tz 
                          + ' and location = \'' + location + '\''
                          + ' and city = \'' + city + '\''
                          + ' and state = \'' + state + '\''
                          + ' and solar_resource_file = \'' + solar_resource_file + '\''
                          + ' and distance = ' + distance; 
      console.log(query);

     mysqlDBConnection.query(query, function(err,rows) {
      if(err) throw err;
      if (rows.length == 0) {
         var post  = {lat : lat, lon : lon, elev : elev, tz : tz,
                      location : location, city : city, state : state,
                      solar_resource_file : solar_resource_file, distance : distance
                     };
         query = mysqlDBConnection.query('INSERT INTO station_info SET ?', post, function(err, result) {
              if (err) throw err;
              station_info_id = result.insertId;
               console.log("station_info_id inserted  " + station_info_id);
          });


      } else {
        station_info_id = rows[0].station_info_Id;
        console.log("station_info_id already registered " + station_info_id);
      }
      insertInput_Output(installationId, ssc_info_id, station_info_id);
    });
  }

  function insertInput_Output(installationId, ssc_info_id, station_info_id){
 

     var tilt = jsonRefData.inputs.tilt;
     var dataset = "'" + jsonRefData.inputs.dataset + "'";
     var timeframe = "'" + jsonRefData.inputs.timeframe + "'";
     var version = "'" + jsonRefData.version + "'";
    // var errors = "'" + JSON.stringify(jsonRefData.inputs.errors) + "'";
    // var warnings = "'" + JSON.stringify(jsonRefData.inputs.warnings) + "'";
  
     var ac_monthly = "'" + JSON.stringify(jsonRefData.outputs.ac_monthly) + "'";
     var poa_monthly = "'" + JSON.stringify(jsonRefData.outputs.poa_monthly) + "'" ;
     var solrad_monthly =  "'" + JSON.stringify(jsonRefData.outputs.solrad_monthly) + "'" ;
     var dc_monthly  = "'" + JSON.stringify(jsonRefData.outputs.dc_monthly) + "'" ;
     var ac_annual =  jsonRefData.outputs.ac_annual  ;
     var solrad_annual =  jsonRefData.outputs.solrad_annual;
     var capacity_factor = jsonRefData.outputs.capacity_factor;
     var ac = "'" + JSON.stringify(jsonRefData.outputs.ac) + "'" ;
     var poa = "'" + JSON.stringify(jsonRefData.outputs.poa) + "'" ;
     var dn = "'" + JSON.stringify(jsonRefData.outputs.dn) + "'";
     var dc =  "'" +  JSON.stringify(jsonRefData.outputs.dc) +  "'" ;
     var df =  "'" +  JSON.stringify(jsonRefData.outputs.df) +  "'" ;
     var tamb =  "'"  + JSON.stringify(jsonRefData.outputs.tamb) +  "'" ;
     var tcell =  "'" +  JSON.stringify(jsonRefData.outputs.tcell) + "'" ;
     var wspd =  "'" + JSON.stringify(jsonRefData.outputs.wspd) + "'";
    
     var resultForDCAvgAndStd = insertDCDataIntoInfluxAndCalculateAvgAndStd(installationId);
     var dc_Avgs = "'" + JSON.stringify(resultForDCAvgAndStd.avgs) + "'";
     var dc_Sds = "'" + JSON.stringify(resultForDCAvgAndStd.sds) + "'";

     setTimeout(function(){process.exit(); }, 40000);


                      
    var query =  'INSERT INTO Input_Output ' +
                 '(ac_monthly, poa_monthly, solrad_monthly, dc_monthly, ac_annual, solrad_annual,' +
                  'capacity_factor, ac, poa, dn, dc, df, tamb, tcell, wspd, dc_Avgs, dc_Sds, ssc_info_id, station_Id, id,' +
                  ' tilt, dataset, timeframe, version) VALUES ('+
                  ac_monthly + ',' + poa_monthly + ',' + solrad_monthly + ',' + dc_monthly + ',' + ac_annual + ',' + solrad_annual + ','
                  + capacity_factor + ',' + ac + ',' + poa + ',' + dn + ',' + dc + ',' + df + ',' + tamb + ',' + tcell + ',' + wspd  + ','
                  + dc_Avgs + ',' + dc_Sds + ','
                  + ssc_info_id + ',' + station_info_id  + ',' + installationId + ',' + tilt + ','  + dataset + ',' + timeframe + ',' + version + ')' ;



     mysqlDBConnection.query(query, function(err, result) {
              if (err) throw err;
               var input_output_id = result.insertId;
               console.log("input_output inserted  " + input_output_id);
      }); 
  }

  function insertDCDataIntoInfluxAndCalculateAvgAndStd (installationId)  {
      var daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
      var year = CONSTANTS.REFYEAR;
      var dcValues = jsonRefData.outputs.dc;
      var count = 0;

      var i = 0, j = 0; 
      var sum = [];
      var variance = [];
      var standardDeviaton = [];
      var result = {};

      for (i = 0 ; i < 24; i++) {
      	 sum[i] = 0;
      	 variance[i] = 0;
      	 standardDeviaton[i] = 0;
      }

      for (i = 0; i < daysInMonth.length; i++) {
        for (j = 1; j <= daysInMonth[i]; j++) {
             var hours = [];
             var dcValuesPerHour = [];

        	   for (var hour = 0; hour < 24; hour++) {
                  hours[hour] = hour;
                  dcValuesPerHour[hour] = dcValues[count++];	
                  sum[hour]+= dcValuesPerHour[hour];
                  
             }  
            influxDB.addMultipleDataPoints(year, i , j, hours, 0, 0, 0, dcValuesPerHour, installationId);
        }
      }

      count = 0;
      for (i = 0; i < daysInMonth.length; i++) {
        for (j = 1; j <= daysInMonth[i]; j++) {
             for (var hour = 0; hour < 24; hour++) {
                  dcValuesPerHour[hour] = dcValues[count++];	
                  var val = Math.abs(sum[hour]/365).toFixed(2) - dcValuesPerHour[hour];
                  variance[hour]+= Math.pow(val , 2);
             }  
        }
      }

      var avgs = [];
      var sds = [];
      for (i = 0 ; i < 24; i++) {
         
         avgs[i] = (sum[i]/365).toFixed(2) ;
         sds[i] =   Math.sqrt((variance[i]/365)).toFixed(2) ; 
       }
       
       result.avgs = avgs;
       result.sds = sds;
       return result;
  }
