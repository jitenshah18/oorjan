

  var generator = require('./LiveDataGenerator.js');
  var influx1 = require('./db.js');
  var comparator = require('./Compare.js');
  var notify = require('./SendEmail.js');
  var CONSTANTS = require('./static/constants.js');
  var express = require('express');
  var mysql = require('mysql');
  var app = express();
  var utility = require('./static/utility.js');
  var http = require('http').createServer(app);
  var Result = require('./Result.js');
  var mysqlDB = require('./mysqlDB.js');
  var mysqlDBConnection;

 mysqlDB.connectToDB(connectToRDBMS);
  

function connectToRDBMS (err, sqlDBConnection) {
  if (err) {
      console.log(CONSTANTS.CANNOT_CONNECT_TO_RDBMS);
      process.exit(CONSTANTS.SYS_EXIT_FAILURE_CODE);
  }
  mysqlDBConnection = sqlDBConnection;
  keepConnectionAlive();
}

  influx1.connectToInfluxDB();
  generator.setDB(influx1);
  generator.setGenerateDCValueFunction(1);
  

  http.listen(CONSTANTS.HTTP_PORT, function() {
      console.log(CONSTANTS.HTTP_LISTENING_MSG + ': ' + CONSTANTS.HTTP_PORT) ;
  });




  app.post('/predictionService/:installationId', function (req, res) {
    
    var installationId = req.params.installationId;
    var date = req.query.date;
    var result;
    
    if(isNaN(installationId)) {
       result = new Result(CONSTANTS.RESULT_FAILURE, { message : CONSTANTS.INVALID_SYSTEM_ID + ' : '+ installationId} );
       sendResponse(res, CONSTANTS.HTTP_OK, result);

    } else if (utility.isDateValidForDataService(date) === false) {
       result = new Result(CONSTANTS.RESULT_FAILURE, { message : CONSTANTS.INVALID_DATE + ' : '+ req.query.date} );
       sendResponse(res, CONSTANTS.HTTP_OK, result);

    } else {
        var id  = parseInt(installationId);
        mysqlDBConnection.query('SELECT * FROM User_MetaData where installationId = ' + id, function(err,rows){
        if(err)  {
          result = new Result(CONSTANTS.RESULT_FAILURE, { message : err} );
          sendResponse(res, CONSTANTS.HTTP_OK, result);
          throw err;
        }
        if (rows.length == 0) {
            result = new Result(CONSTANTS.RESULT_FAILURE, { message : CONSTANTS.INVALID_SYSTEM_ID + ' : '+ installationId} );
            sendResponse(res, CONSTANTS.HTTP_OK, result);
        } else {
            var splitDate = date.split(CONSTANTS.DATE_SEPARATOR);
            
            var year = parseInt(splitDate[2]), day = parseInt(splitDate[0]), month = parseInt(splitDate[1]);
            mysqlDBConnection.query('SELECT dc_Avgs, dc_Sds FROM Input_Output where id = ' + id, function(err,rows){
              
             if(err)  {
                 result = new Result(CONSTANTS.RESULT_FAILURE, { message : err} );
                 sendResponse(res, CONSTANTS.HTTP_OK, result);
                 throw err;
             }    
             var dc_Sds = JSON.parse(rows[0].dc_Sds);
             var dc_Avgs = JSON.parse(rows[0].dc_Avgs);
             //console.log(dc_Sds + "\n"+ dc_Avgs);
             solveForADay(res,year, month, day, id, dc_Avgs, dc_Sds);
           });  
        }
      });
    }    

  })



  app.all('*', function(req, res){
     sendResponse(res, CONSTANTS.HTTP_NOT_FOUND, CONSTANTS.INVALID_API_ENDPOINT);
  });


    function sendResponse (response, httpResponseCode, result) {
        response.status(httpResponseCode).json(result);
    }

   
     function solveForADay(httpResponse, year, originalMonth, day,installationId, dc_Avgs, dc_Sds) {
          
          generator.generateData(year, originalMonth - 1, day, installationId, dc_Avgs, dc_Sds);
          var referenceResults = undefined, particularDayResults = undefined;
          influx1.fetchOutputs(CONSTANTS.REFYEAR, year, originalMonth - 1, day, CONSTANTS.DEFAULT_HOUR ,
                               CONSTANTS.DEFAULT_MINUTE, CONSTANTS.DEFAULT_SECOND, CONSTANTS.DEFAULT_MILLISECOND, installationId, CONSTANTS.REFDAY, fnForRefDay); 

          function fnForAGivenDay(referenceDay, results) {
              particularDayResults = results;
              var result = comparator.compareOutputsFromInfluxDB(referenceResults, particularDayResults);

              var body = utility.prepareEmailBody(result);
              var userResult  = new Result(CONSTANTS.RESULT_SUCCESS, {message : body, hours : result.data} );
              sendResponse(httpResponse, CONSTANTS.HTTP_OK, userResult);
              notify.notifyUser(body, CONSTANTS.EMAIL_HOUR, CONSTANTS.EMAIL_MIN);    
          }  

          function fnForRefDay(referenceDay, results, year, month, day, hour, min, seconds, milliseconds, installationId) {
               if(referenceDay == true) {
                   referenceResults = results;
                   influx1.fetchOutputs(CONSTANTS.REFYEAR, year, month, day, hour, min, seconds, milliseconds, installationId, !CONSTANTS.REFDAY, fnForAGivenDay);
               } 
         }
     }


  function keepConnectionAlive () {
    setInterval(function () {
        mysqlDBConnection.query('SELECT * FROM User_MetaData where installationId = -1', function(err,rows){
          if(err) throw err;
            // console.log("KeepAlive Check");
        });  
    }, CONSTANTS.RDBMS_KEEPALIVE_TIMER);

  }
    
  process.on('uncaughtException', function (err) {
        console.log('Caught exception: ' + err);
  });   


      