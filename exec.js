
const generator = require('./LiveDataGenerator.js');
const influx1 = require('./db.js');
const comparator = require('./Compare.js');
const notify = require('./SendEmail.js');
const CONSTANTS = require('./static/constants.js');
var readline = require('readline');




influx1.connectToInfluxDB();
generator.setDB(influx1);
generator.setGenerateDCValueFunction(1);

console.log(CONSTANTS.INPUT_ACCEPT_MSG);

var read = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


read.on('line', function(line){
    //console.log("this comes from the terminal " + line);
    line = line.trim();
    var inputParams = line.split(",");
    var acceptedInputFormat = false; 
    if (inputParams.length == 4) {
        acceptedInputFormat = true;
        
            for (var i = 0; i < inputParams.length; i++) {

                 if (isNaN(parseInt(inputParams[i].trim()))) {
                     console.log(CONSTANTS.INPUT_INVALID_MSG);
                     acceptedInputFormat = false;
                     break;
                 }
     
            }
           
     }  else {
            acceptedInputFormat = false;
            console.log(CONSTANTS.INPUT_PARAMS_INVALID_MSG);
     }
    // console.log(acceptedInputFormat);

    if (acceptedInputFormat) {
        var year =  parseInt(inputParams[0]);
        var month = parseInt(inputParams[1]);
        var day = parseInt(inputParams[2]);
        var installationId = parseInt(inputParams[3]);

        if (year === CONSTANTS.REFYEAR) {
            console.log(CONSTANTS.INPUT_YEAR_INVALID_MSG);
            return;
        }

        if (installationId !== 1) {
            console.log(CONSTANTS.INPUT_INSTALLATION_ID_INVALID_MSG);
            return;
        }
        

        generator.generateData(year, month, day, installationId);
        // this is 24 Dec, 2017 - month takes values between 0 and 11.
        referenceResults = undefined ; particularDayResults = undefined;
        influx1.fetchOutputs(CONSTANTS.REFYEAR, year, month, day, installationId, 0, 0, 0, 1, CONSTANTS.REFDAY, callbackFnForRefDay); 
        console.log(CONSTANTS.INPUT_ACCEPT_MSG);
     }
});

 
  var referenceResults = undefined, particularDayResults = undefined;

    function callbackFnForADay(referenceDay, results) {
        var check = false;
         particularDayResults = results;
         //console.log(particularDayResults);
         var result = comparator.compareOutputsFromInfluxDB(referenceResults, particularDayResults);
         console.log(result);
         notify.notifyUser(result);    
    }  

    function callbackFnForRefDay(referenceDay, results, year, month, day, hour, min, seconds, milliseconds, installationId) {
            if(referenceDay == true) {
                referenceResults = results;
                  //console.log(!CONSTANTS.REFDAY);
                 influx1.fetchOutputs(CONSTANTS.REFYEAR, year, month, day, hour, min, seconds, milliseconds, installationId, !CONSTANTS.REFDAY, callbackFnForADay);
             } 


    }