var exports = module.exports = {};

var CONSTANTS = require('./static/constants.js');
var aws = require('aws-sdk');
var awsConfig = require('./static/awsConfig.js');
var nodeSchedule = require('node-schedule');

aws.config.region = awsConfig.region;
aws.config.update({accessKeyId: awsConfig.accessKeyId, secretAccessKey: awsConfig.secretAccessKey});
var ses = new aws.SES({apiVersion: CONSTANTS.AWS_SES_API_VERSION});


exports.notifyUser = function (body, hour, min, seconds) {
 
 var now = new Date();
 var newHour, newMinutes, newSeconds;
 if (hour === undefined) {
    newHour = now.getHours();
    newMinutes = now.getMinutes();
    newSeconds = now.getSeconds() + CONSTANTS.SEND_EMAIL_IN_SECONDS;
 } else {
    newHour = hour;
    newMinutes = (min === undefined) ? 0 : min;
    newSeconds = (seconds === undefined) ? 0 : seconds;

   var timeToSendByApp = new Date(now.getFullYear(), now.getMonth(), now.getDate(), newHour, newMinutes, newSeconds, CONSTANTS.DEFAULT_MILLISECOND);
   //console.log("timeToSendByApp" + timeToSendByApp);
   now = new Date();
   if (now.getTime() - timeToSendByApp.getTime() > 0) {
       //console.log(" Time has areadyg gone"); 
       newHour = now.getHours();
       newMinutes = now.getMinutes();
       newSeconds = now.getSeconds() + CONSTANTS.SEND_EMAIL_IN_SECONDS;
      // console.log("Sending at current time + 40 seconds as the time has passed");
   } 
 }

 var sendEmailDate = new Date(now.getFullYear(), now.getMonth(), now.getDate(), newHour, newMinutes, newSeconds, CONSTANTS.DEFAULT_MILLISECOND);
 console.log("Email will be sent at : " + sendEmailDate);

nodeSchedule.scheduleJob(sendEmailDate, function(){
       // console.log('Sending Email' + newHour + " " + newMinutes + " " + newSeconds);
        var to = [awsConfig.informUserEmail];
        var from = CONSTANTS.ADMIN_EMAIL_ACC;
        var title = CONSTANTS.EMAIL_TITLE;
        

        ses.sendEmail( { 
           Source: from, 
           Destination: { ToAddresses: to },
           Message: {
               Subject: {
                  Data: title
               },
               Body: {
                   Text: {
                     Data: body,
                  }
                }
           }
        }
        , function(err, data) {
            if(err) throw err
            console.log('Email sent');
           // console.log(data);
         });

    });
}

