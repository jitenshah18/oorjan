exports = module.exports = {};


var Influx = require('influx');
var config = require('./static/dbConfig.js');
var utility = require('./static/utility.js');
var influx1;

exports.connectToInfluxDB = function () {
    //console.log(config);
    influx1 = new Influx.InfluxDB({
        host: config.host,
        port: config.port,
        protocol: config.protocol,
        username: config.username,
        password: config.password,
        database : config.database,

        schema: [

          {
             measurement : config.measurement,
             fields: {
                'dcValue' : Influx.FieldType.FLOAT
            },
            tags:[
                'installationId'
            ]

          }
        ]
    });
}

exports.addDataPoint = function (point) {
   
  var year  = point.year;
  var month = point.month;
  var day = point.day;
  var minutes = point.minutes;
  var hours = point.hours; 
  var mins = point.mins;
  var seconds = point.seconds;
  var milliseconds = point.milliseconds;
  var dcValue = point.dcValue;
  var installationId = point.installationId;
  
   var dayObject = new Date(Date.UTC(year, month, day, hours, mins, seconds, milliseconds));
   //console.log(config.measurement + " " + installationId + " " + dcValue + " " + dayObject);
   influx1.writePoints([
         {
                measurement: config.measurement,
                tags: { installationId : installationId},
                fields: { dcValue : dcValue },
                timestamp: dayObject 
         } 
    ]).then (
             
             function() {
              //console.log("Success");
                /*console.log('Success for year: ' + year + ' momth: ' + month
                         + ' day: ' + day + ' hours: ' + hours + ' minutes : ' + mins
                         + ' seconds: ' + seconds + ' milliseconds: ' + milliseconds
                         + ' dcValue: ' + dcValue + ' installationId: ' + installationId);*/
             }

     ).catch(

         function(reason) {
              console.log('Failed for reason = ' + reason + ' year: ' + year + ' momth: ' + month
                         + ' day: ' + day + ' hours: ' + hours + ' minutes : ' + mins
                         + ' seconds: ' + seconds + ' milliseconds: ' + milliseconds
                         + ' dcValue: ' + dcValue + ' installationId: ' + installationId);
              
         });
  }

  exports.addMultipleDataPoints = function (year, month, day, hours, minutes, seconds, milliseconds, dcVal, installationId) {
 
  var points = [];
 // console.log("hours.length" + hours.length);
   for (var i = 0; i < hours.length; i++) {
        var tday = new Date(Date.UTC(year, month, day, hours[i], minutes, seconds, milliseconds));
        points[i] = {
                measurement: config.measurement,
                tags: { installationId : installationId},
                fields: { dcValue : dcVal[i] },
                timestamp: tday
         };
   }     
   
   influx1.writePoints(
         points
    ).then (
             function() {
                console.log('Bulk Success');           
             }

     ).catch(
         function(reason) {
              console.log('Bulk Failed' + reason);
         });
  }

  exports.fetchOutputs = function  (refyear, year, month, day, hour, min, seconds, milliseconds, installationId, referenceDay, callback) {
     var findYear = referenceDay == true ? refyear : year;
     var startTime = utility.formatTimeForQuery(findYear, month, day,  0, min, seconds, milliseconds);
     var   endTime = utility.formatTimeForQuery(findYear, month, day, 23, min, seconds, milliseconds);
     
    
     //console.log("Decide Year " + month  + "  " + month);
     var query = "select * from " + config.measurement + " WHERE installationId = '" + installationId + "' AND time >= '" +  startTime  + "' AND time <='" + endTime + "'";
     console.log(query);
      influx1.query(query).then(results => {
             callback(referenceDay, results, year, month, day, hour, min, seconds, milliseconds, installationId);
             
      });
      
  }
