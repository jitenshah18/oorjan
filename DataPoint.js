var DataPoint = function(year, month, day, hours, mins, seconds, milliseconds, dcValue, installationId) {
  this.year = year;
  this.month = month;
  this.day = day;
  this.hours = hours;
  this.mins = mins;
  this.seconds = seconds;
  this.milliseconds = milliseconds,
  this.dcValue = dcValue;
  this.installationId = installationId;
};

DataPoint.prototype.toString = function() {
   return      'year:' + this.year +
               ' month:' + this.month +
               ' day:' + this.day +
               ' hours:' + this.hours +
               ' mins:' + this.mins +
               ' seconds:' + this.seconds +
               ' milliseconds:' + this.milliseconds +
               ' dcValue:' + this.dcValue +
               ' installationId:' + this.installationId;
};

DataPoint.prototype.equals = function(otherDataPoint) {
  
  return otherDataPoint != null 
            && this.year === otherDataPoint.year
            && this.month === otherDataPoint.month   
            && this.hours === otherDataPoint.hours
            && this.day === otherDataPoint.day
            && this.mins === otherDataPoint.mins
            && this.seconds === otherDataPoint.seconds
            && this.milliseconds === otherDataPoint.milliseconds
            && this.dcValue === otherDataPoint.dcValue
            && this.installationId === otherDataPoint.installationId;

 };

 var exports = module.exports = DataPoint;