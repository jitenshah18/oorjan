var exports = module.exports = {};
var CONSTANTS = require ('./static/constants.js');
var DataPoint = require('./DataPoint.js');
  
 function compareOutputsFromInfluxDBToDataPoint(results) {
  var data = [];
  if (results != undefined && results != null ) {
     var resultCount = results.length;
     var i = 0; 
     for(i = 0; i < resultCount; i++) {
          var point = results[i];
          //console.log(refData);
          var refDate = new Date(point.time._nanoISO); 
          //console.log(refData.dcValue + " " + refData.installationId);   
          data[i] =  new DataPoint(refDate.getUTCFullYear(), refDate.getUTCMonth(), refDate.getUTCDay(),
                            refDate.getUTCHours(), refDate.getUTCMinutes(), refDate.getUTCSeconds(),
                            refDate.getUTCMilliseconds(), point.dcValue, point.installationId);
     }
     

  } else {
    console.log("Results is either null or not defined");
  }
  return data;

 }       

  exports.compareOutputsFromInfluxDB = function (refResults, dayResults) {
  
   var refData = compareOutputsFromInfluxDBToDataPoint(refResults);
   var dayData = compareOutputsFromInfluxDBToDataPoint(dayResults);
   
   return this.compareOutputs(refData, dayData);

  }  

  exports.compareOutputs = function (refResults, dayResults) {
     var result = {};
     var data = [];
     result.message = CONSTANTS.SUCC_MESSAGE;
     result.data = data;
     
     var count = 0;
      
     if (refResults == undefined) {
         console.log("refResults == undefined");
         result.message = CONSTANTS.ERR_MESSAGE;
     } else if (dayResults != null && dayResults !=  undefined) {
         
         
         var refResultCount = refResults.length;
         var dayResultCount = dayResults.length;
         
        //console.log("refResultCount " + refResultCount+ " dayResultCount " + dayResultCount);
     
         var i = 0;
         var j = 0;
         var reportTime, refDCValue, dayDCValue;
         for(i = 0; i < refResultCount && j < dayResultCount; i++) {
            
             var refDataPoint = refResults[i];
             var dayDataPoint = dayResults[j];
             //console.log("i "  + i + " dayDataPoint " +  dayDataPoint);
             refDCValue = refDataPoint.dcValue;
              if (dayDataPoint) {
                 
                  if ((refDataPoint.hours == dayDataPoint.hours) && (refDataPoint.mins == dayDataPoint.mins)) {
                         
                         dayDCValue = dayDataPoint.dcValue;

                       if (checkReportingCondition(dayDCValue, refDCValue)) {
                           reportTime = refDataPoint.hours + ":" + refDataPoint.mins;
                           data[count++] = reportTime;
                       }
                       j++;
                       
                  }  else if (refDCValue != 0) {

                        reportTime = refDataPoint.hours + ":" + refDataPoint.mins;
                        //console.log(refDataPoint);
                        //console.log("It comes from here "  +  "refDCValue " + refDCValue +reportTime);
                        data[count++] = reportTime;
                  } 
           }   
         } // for loop ends
     
     
       for ( i; i < refResultCount; i++) {
             var refDataPoint = refResults[i];
             refDCValue = refDataPoint.dcValue;
            if (refDataPoint && (refDCValue != 0)) {
                 reportTime = refDataPoint.hours + ":" + refDataPoint.mins;
                 data[count++] = reportTime;
            }
            
         }  

          
       } else {
           console.log("else part == undefined");
          result.message = CONSTANTS.ERR_MESSAGE;
       }
       
     return result;
  }

  function checkReportingCondition (dayDCValue, refDCValue) {
      return dayDCValue < CONSTANTS.CHECKCRITERIAPERCENT * refDCValue;
  }
