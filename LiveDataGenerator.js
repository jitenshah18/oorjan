var exports = module.exports = {};
var db;
var utility = require('./static/utility.js');
var DataPoint = require('./DataPoint.js');
var CONSTANTS = require('./static/constants.js');
var gaussian = require('gaussian');


 exports.setDB = function (DB) {
    db = DB;
  }

  /**
   * There will be environment variables which will let 
   * us know if Code is currently curring in Production
   * or Test Environment
   */
  exports.generateDCValue;
  exports.setGenerateDCValueFunction = function (production) {
    if (production == 1) {
        this.generateDCValue = this.generateDCValueAsPerTimeWithAvgAndSD;
    }
    else {
       this.generateDCValue = returnSameDCValue;
    }
  }

 
 exports.generateData = function(year, month, day, installationId, dc_Avgs, dc_Sds) {
    var i = 0;
    var data = [];
    var seconds = 0, milliseconds = 0;
    //console.log(dc_Avgs + " " + dc_Sds);
    for (i = 0; i < 24; i++) {
       
       var hours = i;
       var mins = 0;
       
       var dcValue = this.generateDCValue(hours, mins, dc_Avgs, dc_Sds);
       data[i] =  new DataPoint(year, month, day, hours, mins, seconds, milliseconds, dcValue, installationId);
       //console.log(data[i]);
       db.addDataPoint(data[i]);
     }  
     return data;
}


exports.generateDataWithDatesAndDCValues = function(year, month, day, dcValues, installationId) {
    var i = 0;
    var data = [];
    for (i = 0; i < 24; i++) {
       
       var hours = i;
       var mins = 0;
       data[i] = new DataPoint(year, month, day, hours, mins, 0, 0, dcValues[i], installationId);
       db.addDataPoint(data[i]);
     }  
     return data;
}

exports.generateARecordWithDateAndDCValue = function(year, month, day, hours, mins, seconds, milliseconds, dcValue, installationId) {
       var mins = 0;
       var data = new DataPoint(year, month, day, hours, mins, 0, 0, dcValue, installationId);
       db.addDataPoint(data);
       return data;
}

 /*exports.generateDCValueAsPerTime  = function (hour, min) {
   const SYSTEMCAPACITYINKWATTS = 10;
   const SYSTEMCAPACITYINWATTS = SYSTEMCAPACITYINKWATTS * 1000;
   const MAXCAPACITYPERCENT = 90;
   const MINUTEINONEHOUR = 60;
   
        if ((hour >= 0 && hour <= 6) || (hour >= 18 && hour <=23))  {
            return 0;
        }

        var totalMin = hour * MINUTEINONEHOUR + min;
        var percentDC = 0; 
       
        var dcValueProduce = (MAXCAPACITYPERCENT * SYSTEMCAPACITYINWATTS / 100);
        
              
        switch(true) {
             case (totalMin >= (7 * MINUTEINONEHOUR) && totalMin < (8 * MINUTEINONEHOUR)) : 
                     percentDC = utility.getRandomBetween(3.9, 4.2);
                     break;
          
             case (totalMin >= (8 * MINUTEINONEHOUR) && totalMin < (9 * MINUTEINONEHOUR)) :  
                    percentDC = utility.getRandomBetween(32.7, 33);
                     break;
                     
             case (totalMin >= (9 * MINUTEINONEHOUR) && totalMin < (10 * MINUTEINONEHOUR)) : 
                    percentDC = utility.getRandomBetween(54, 54.77);
                    break;
                     
             case (totalMin >= (10 * MINUTEINONEHOUR) && totalMin < (11 * MINUTEINONEHOUR)) : 
                     percentDC = utility.getRandomBetween(68, 68.3);
                     break;
                     
             case (totalMin >= (11 * MINUTEINONEHOUR) && totalMin < (12 * MINUTEINONEHOUR)) :  
                     percentDC = utility.getRandomBetween(77, 77.4);
                     break;                
                             
             case (totalMin >= (12 * MINUTEINONEHOUR) && totalMin < (12.25 * MINUTEINONEHOUR)): 
                      percentDC = utility.getRandomBetween(83.1, 84);
                      break;
                      
             case (totalMin >= (13 * MINUTEINONEHOUR) && totalMin < (14 * MINUTEINONEHOUR)) : 
                     percentDC = utility.getRandomBetween(80.66, 80.9);
                     break;
              
              case (totalMin >= (14 * MINUTEINONEHOUR) && totalMin < (15 * MINUTEINONEHOUR)) : 
                     percentDC = utility.getRandomBetween(77.5, 78);
                     break;
                     
             case (totalMin >= (15 * MINUTEINONEHOUR) && totalMin < (16 * MINUTEINONEHOUR)) : 
                     percentDC = utility.getRandomBetween(61.1, 61.5);
                     break;
                     
             case (totalMin >= (16 * MINUTEINONEHOUR) && totalMin < (17 * MINUTEINONEHOUR)) : 
                     percentDC = utility.getRandomBetween(40, 40.3);
                     break;
                     
             case (totalMin >= (17 * MINUTEINONEHOUR) && totalMin < (18 * MINUTEINONEHOUR)) : 
                     percentDC = utility.getRandomBetween(11.8, 12);
                     break;                              
                      
             default : 
                    console.log("default case");
                    percentDC = 0;   
    
        }
        return (percentDC * dcValueProduce / 100).toFixed(2);
   
   }
    */
   function returnSameDCValue () {
        return CONSTANTS.STANDARD_DC_VALUE;
   }

  exports.generateDCValueAsPerTimeWithAvgAndSD  = function (hour, min, avgs, stdDeviations) {
   
    var avg = parseFloat(avgs[hour]);
    var stdDeviation = parseFloat(stdDeviations[hour]);
    if (avg === 0.00 && stdDeviation === 0.00)
        return 0.00;
    var d1 = gaussian(avg, stdDeviation);
    var val = d1.ppf(Math.random());
    //console.log(avgs[hour] + " " + stdDeviations[hour] + " " + val);
    return val;
 }   


  