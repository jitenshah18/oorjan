const  generator = require('./../LiveDataGenerator.js');
const assert = require('assert');
const DataPoint = require('./../DataPoint.js');
const db = require('./mockDB.js');
const CONSTANTS = require('../static/constants.js');

generator.setDB(db);
generator.setGenerateDCValueFunction(0);

function testGenerateDateNumberOfRecordsReturned() {

	var year = 2016;
	var month =  3;
	var day =  24;
	var installationId =  1;
	
    var expectedData = [];
    var i = 0;
    
    
	var actualData = generator.generateData(year, month, day, installationId);
    assert.equal(actualData.length, 24, "Did not generate 24 points");
    console.log("testGenerateDateNumberOfRecordsReturned passed");
}

function testGenerateDateForCorrectValuesGenerated() {

	var year = 2017;
	var month =  3;
	var day =  24;
	var dcValue = CONSTANTS.STANDARD_DC_VALUE, min = 0, seconds = 0, milliseconds = 0;
	var installationId =  1;
	
    var expectedData = [];
    var i = 0;
    
    for(i = 0; i < 24; i++) {
        expectedData[i] = new DataPoint (year, month, day, i, min, seconds, milliseconds, dcValue, installationId);
    }

	var actualData = generator.generateData(year, month, day, installationId);

    for (i = 0; i < 24; i++) {
    	assert(actualData[i].equals(expectedData[i]), "testGenerateDateForCorrectValuesGenerated : actualObject " + actualData[i].toString() 
                                                      + " expectedObject " + expectedData[i].toString());
     }
     console.log("testGenerateDateForCorrectValuesGenerated passed");
}

function testGenerateDataWithDatesAndDCValues() {

    var year = 2018;
    var month =  3;
    var day =  24;
    var  min = 0, seconds = 0, milliseconds = 0;
    var installationId =  1;
    var dcValues = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240];
    var expectedData = [];
    var i = 0;
    
    for(i = 0; i < 24; i++) {
        expectedData[i] = new DataPoint (year, month, day, i, min, seconds, milliseconds, dcValues[i], installationId);
    }

    var actualData = generator.generateDataWithDatesAndDCValues(year, month, day, dcValues, installationId);

    for (i = 0; i < 24; i++) {
        assert(actualData[i].equals(expectedData[i]), "testGenerateDataWithDatesAndDCValues : actualObject " + actualData[i].toString() 
                                                      + " expectedObject " + expectedData[i].toString());
     }
     console.log("testGenerateDataWithDatesAndDCValues passed");
}

function testGenerateARecordWithDateAndDCValue() {

    var year = 2019;
    var month =  3;
    var day =  24;
    var dcValue = 0, min = 0, seconds = 0, milliseconds = 0;
    var installationId =  1;
    var hours = 9;
    var mins = 0, seconds = 0, milliseconds = 0;
    
    var  expectedData = new DataPoint (year, month, day, hours, min, seconds, milliseconds, dcValue, installationId);
    var actualData = generator.generateARecordWithDateAndDCValue(year, month, day, hours, mins, seconds, milliseconds, dcValue, installationId);

    assert(actualData.equals(expectedData), "testGenerateARecordWithDateAndDCValue : actualObject " + actualData.toString() 
                                                      + " expectedObject " + expectedData.toString());
     console.log("testGenerateARecordWithDateAndDCValue passed");
}



testGenerateDateNumberOfRecordsReturned();
testGenerateDateForCorrectValuesGenerated();
testGenerateDataWithDatesAndDCValues();
testGenerateARecordWithDateAndDCValue();