var exports = module.exports = {};
var email = require('./../SendEmail.js'); 
var CONSTANTS = require('./../static/constants.js');
var utility = require('./../static/utility.js');


var result = {};
result.message = CONSTANTS.SUCC_MESSAGE;
result.data = ['15.0', '16.0'];

function testSendEmailAtSpecificTime () {
      var hour = 18;
      var min = 47;
      var seconds = 23;
      var body = utility.prepareEmailBody(result);
      email.notifyUser(result, hour, min, seconds);
     console.log("testSendEmailAtSpecificTime : Check your email ");
}

function testSendEmailAtSpecificTimeWithNoSecondsMentioned () { 

   var hour = 18;
   var min = 47;
   var body = utility.prepareEmailBody(result);   
   email.notifyUser(body, hour, min);
   console.log("testSendEmailAtSpecificTimeWithNoSecondsMentioned : Check your email ");
}	

function testSendEmailAtSpecificTimeWithNoSecondsAndMinutesMentioned () {
   var hour = 19;
   var body = utility.prepareEmailBody(result);
   email.notifyUser(body, hour);
   console.log("testSendEmailAtSpecificTimeWithNoSecondsAndMinutesMentioned : Check your email ");
}	

function testSendEmailWithoutSpecifyingTime() {
   var body = utility.prepareEmailBody(result);
   email.notifyUser(body);
   console.log("testSendEmailWithoutSpecifyingTime : Check your email ");
}	

testSendEmailAtSpecificTime();
testSendEmailAtSpecificTimeWithNoSecondsMentioned();
testSendEmailAtSpecificTimeWithNoSecondsAndMinutesMentioned();
testSendEmailWithoutSpecifyingTime();