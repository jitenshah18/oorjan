const  compare = require('./../Compare.js');
const assert = require('assert');
const DataPoint = require('./../DataPoint.js');
const CONSTANTS = require('./../static/constants.js');


function testCompareOutputsWithEachHaving24Results () {

	var year = 2017;
	var month =  3;
	var day =  24;
	var dcValue = CONSTANTS.STANDARD_DC_VALUE, min = 0, seconds = 0, milliseconds = 0;
	var installationId =  1;
	var refResults = [], dayResults = [];
    var expectedData = []; var count = 0;
    var i = 0;
    
    for(i = 0; i < 24; i++) {
        refResults[i] = new DataPoint (year, month, day, i, min, seconds, milliseconds, dcValue, installationId);
 
        var dcValueForADay;
        if (i % 2 == 0) {
           dcValueForADay = (1 - CONSTANTS.CHECKCRITERIAPERCENT) * dcValue;
           dayResults[i] = new DataPoint (year, month, day, i, min, seconds, milliseconds, dcValueForADay, installationId);
           expectedData[count++] =  i + ":" + min;
        } else {
        	dcValueForADay = 1.2 * dcValue;
        	dayResults[i] = new DataPoint (year, month, day, i, min, seconds, milliseconds, dcValueForADay, installationId);
        }
    }
    var actualData = [];
    var result = compare.compareOutputs(refResults, dayResults);
    if (result != null && result != undefined) {
        actualData = result.data;
    }
    assert.equal(actualData.length, 12, "No Records Returned. Expected " + expectedData.length + " records to be returned");
    
    for (i = 0; i < expectedData.length; i++) {
    	assert.equal(expectedData[i], actualData[i], "testCompareOutputsWithEachHaving24Results : actualObject " + actualData[i] 
                                                      + " expectedObject " + expectedData[i]);
     }
     console.log("testCompareOutputsWithEachHaving24Results passed");
}


function testCompareOutputsWithDayDataEmpty () {

	var year = 2017;
	var month =  3;
	var day =  24;
	var dcValue = CONSTANTS.STANDARD_DC_VALUE, min = 0, seconds = 0, milliseconds = 0;
	var installationId =  1;
	var refResults = [], dayResults = [];
    var expectedData = []; var count = 0;
    var i = 0;
    
    for(i = 0; i < 24; i++) {
        refResults[i] = new DataPoint (year, month, day, i, min, seconds, milliseconds, dcValue, installationId);
        expectedData[count++] = i + ":" + min;
    }
    var actualData = [];
    var result = compare.compareOutputs(refResults, dayResults);
    if (result != null && result != undefined) {
        actualData = result.data;
    }
    //console.log(result);

    assert.equal(actualData.length, 24, "No Records Returned. Expected " + expectedData.length + " records to be returned");
    
    for (i = 0; i < expectedData.length; i++) {
    	assert.equal(expectedData[i], actualData[i], "testCompareOutputsWithEachHaving24Results : actualObject " + actualData[i] 
                                                      + " expectedObject " + expectedData[i]);
     }
     console.log("testCompareOutputsWithDayDataEmpty passed");
}

function testCompareOutputsWithDayDataMissingForFewHours () {

	var year = 2017;
	var month =  3;
	var day =  24;
	var dcValue = CONSTANTS.STANDARD_DC_VALUE, min = 0, seconds = 0, milliseconds = 0;
	var installationId =  1;
	var refResults = [], dayResults = [];
    var expectedData = []; var count = 0;
    var i = 0, j = 0;
    
    for(i = 0; i < 24; i++) {
        var dcValueForADay;
        if (i % 2 == 0) {
           refResults[i] = new DataPoint (year, month, day, i, min, seconds, milliseconds, dcValue, installationId);

           dcValueForADay = 1.2 * dcValue;
	       dayResults[j] = new DataPoint (year, month, day, i, min, seconds, milliseconds, dcValueForADay, installationId);

           dcValueForADay = (1 - CONSTANTS.CHECKCRITERIAPERCENT) * dcValue;
           if(i % 4 == 0) {
              dayResults[j] = new DataPoint (year, month, day, i, min, seconds, milliseconds, dcValueForADay, installationId);
              expectedData[count++] =  i + ":" + min;
          }
          j++;

        } else {
        	refResults[i] = new DataPoint (year, month, day, i, min, seconds, milliseconds, 0, installationId);
        }
        
        
    }
   // console.log(dayResults.length);
    var actualData = [];
    var result = compare.compareOutputs(refResults, dayResults);
    if (result != null && result != undefined) {
        actualData = result.data;
    }
    //console.log(actualData);
    assert.equal(actualData.length, 6, "No Records Returned. Expected " + expectedData.length + " records to be returned");
    
    for (i = 0; i < expectedData.length; i++) {
    	assert.equal(expectedData[i], actualData[i], "testCompareOutputsWithEachHaving24Results : actualObject " + actualData[i] 
                                                      + " expectedObject " + expectedData[i]);
     }
     console.log("testCompareOutputsWithDayDataMissingForFewHours passed");
}



testCompareOutputsWithEachHaving24Results();
testCompareOutputsWithDayDataEmpty();
testCompareOutputsWithDayDataMissingForFewHours();